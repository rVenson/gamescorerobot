<?php

require_once("igdb.php");
require_once(".config/auth.php");
define('TELEGRAM_API_URL', 'https://api.telegram.org/bot'.TELEGRAM_BOT_TOKEN.'/');

$database = new IGDB();

function apiRequestJson($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  $parameters["method"] = $method;

  $handle = curl_init(TELEGRAM_API_URL);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
  curl_setopt($handle, CURLOPT_POST, true);
  curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
  curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

  return exec_curl_request($handle);
}

function exec_curl_request($handle) {
  $response = curl_exec($handle);

  if ($response === false) {
    $errno = curl_errno($handle);
    $error = curl_error($handle);
    error_log("Curl returned error $errno: $error\n");
    curl_close($handle);
    return false;
  }

  $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
  curl_close($handle);

  if ($http_code >= 500) {
    // do not wat to DDOS server if something goes wrong
    sleep(10);
    return false;
  } else if ($http_code != 200) {
    $response = json_decode($response, true);
    error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
    if ($http_code == 401) {
      throw new Exception('Invalid access token provided');
    }
    return false;
  } else {
    $response = json_decode($response, true);
    if (isset($response['description'])) {
      error_log("Request was successfull: {$response['description']}\n");
    }
    $response = $response['result'];
  }

  return $response;
}

function processCommand($command){
  global $database;

  $command_name = explode(' ',trim($command['text']))[0];
  $command_text = substr(strstr($command['text']," "), 1);
  $message_id = $command['message_id'];
  $chat_id = $command['chat']['id'];

  $previewWebPage = true;

  if($command_name == "/game"){
    $result = $database->searchByName($command_text);
  } else if ($command_name == "/list"){
    $result = $database->getBestGameListByTheme($command_text);
    $previewWebPage = false;
  } else if ($command_name == "/video"){
    $result = $database->getTrailerByName($command_text);
    $previewWebPage = true;
    $message = 'https://youtube.com/watch?v=' . $database->getTrailerByName($command_text)[0][videos][0]['video_id'];
    apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => $message, "disable_web_page_preview" => !$previewWebPage));
    return false;
  }

  if($result == NULL){
    apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => "I can't help you this time"));
  } else {
    $number = 1;
    foreach ($result as $key => $value) {
      $game .= '['.$number.'] ' . $value['name']
      . "\n\n".$value['summary'].'\n\n'
      . "\n====================\n"
      . "Market Rating: ".intval($value['aggregated_rating'])."%"
      . "\n"
      . "Community Rating: ".intval($value['rating'])."%"
      . "\nRelease Date: ".date('d/M/Y', $value['first_release_date'])
      . "\nTime to Beat: ".$value['time_to_beat']
      . "\n".$value['url']
      . "\n\n";

      $number ++;
    }
    apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => $game, "disable_web_page_preview" => !$previewWebPage));
  }
}

$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
  // receive wrong update, must not happen
  exit;
}

if (isset($update["message"])) {
  if ($update['message']['text'][0] == "/") {
      processCommand($update["message"]);
  }
}
?>
